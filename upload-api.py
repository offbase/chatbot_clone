
# from pandasai.llm.openai import OpenAI
# from pandasai import PandasAI
# import pickle

from PyPDF2 import PdfReader

import os
import pandas as pd
import csv
import io
import pickle
# from streamlit_extras.add_vertical_space import add_vertical_space

# from langchain.text_splitter import RecursiveCharacterTextSplitter
# from langchain.embeddings.openai import OpenAIEmbeddings
# from langchain.vectorstores import FAISS
# from langchain.llms import OpenAI
# from langchain.chains.question_answering import load_qa_chain
# from langchain.callbacks import get_openai_callback

openai_api_key = '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'

# using flask_restful
from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from flask_cors import CORS, cross_origin

from langchain.agents import create_csv_agent
from langchain.llms import OpenAI
from langchain.document_loaders import PyPDFLoader
from langchain.vectorstores import FAISS

from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.embeddings.openai import OpenAIEmbeddings
from langchain.vectorstores import FAISS
from langchain.chains.question_answering import load_qa_chain
from langchain.callbacks import get_openai_callback


  
# creating the flask app
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

# creating an API object
api = Api(app)
  
# making a class for a particular resource
# the get, post methods correspond to get and post requests
# they are automatically mapped by flask_restful.
# other methods include put, delete, etc.
class Pdf(Resource):
  
    # corresponds to the GET request.
    # this function is called whenever there
    # is a GET request for this resource
    def get(self):
  
        return jsonify({'message': 'Pdf world'})
  
    # Corresponds to POST request
    def post(self):
        try:
            FILE_PATH = ""
            prompt = ""

            if request.form.get("prompt"):
                prompt = request.form.get("prompt")
            if request.form.get("FILE_PATH"):
                FILE_PATH = request.form.get("FILE_PATH")
            
            if FILE_PATH:
                pdf_reader = PdfReader(FILE_PATH)
        
                text = ""
                for page in pdf_reader.pages:
                    text += page.extract_text()
        
                text_splitter = RecursiveCharacterTextSplitter(
                    chunk_size=1000,
                    chunk_overlap=200,
                    length_function=len
                    )
                chunks = text_splitter.split_text(text=text)
        
                # # embeddings
                store_name = FILE_PATH
                # st.write(f'{store_name}')
                # st.write(chunks)
        
                if os.path.exists(f"{store_name}.pkl"):
                    with open(f"{store_name}.pkl", "rb") as f:
                        VectorStore = pickle.load(f)
                    # st.write('Embeddings Loaded from the Disk')s
                else:
                    embeddings = OpenAIEmbeddings(openai_api_key=openai_api_key)
                    VectorStore = FAISS.from_texts(chunks, embedding=embeddings)
                    with open(f"{store_name}.pkl", "wb") as f:
                        pickle.dump(VectorStore, f)
        

                # Accept user questions/query
                query = prompt
                print(query)
                if query:
                    docs = VectorStore.similarity_search(query=query, k=3)
                    print(docs)
                    llm = OpenAI(openai_api_key=openai_api_key)
                    chain = load_qa_chain(llm=llm, chain_type="stuff")
                    with get_openai_callback() as cb:
                        print(cb)
                        res = chain.run(input_documents=docs, question=prompt)
                        print(res, "response")
                # loader = PyPDFLoader(FILE_PATH)
                # pages = loader.load_and_split()
                # faiss_index = FAISS.from_documents(pages, OpenAIEmbeddings(openai_api_key=openai_api_key))
                # docs = faiss_index.similarity_search(prompt, k=2)
                # for doc in docs:
                #     print(str(doc.metadata["page"]) + ":", doc.page_content[:300])
                #     res = str(doc.metadata["page"]) + ":", doc.page_content[:300]
                # agent = create_csv_agent(
                #                 OpenAI(openai_api_key=openai_api_key, temperature=0),
                #                 FILE_PATH,
                #                 verbose=True)
                # res = agent.run(prompt)
                return res, 201

            # Upload CSV file with utf encoding
            file = request.files['file']
            FILE_PATH = './' + file.filename

            # bytes_data = file.read()
            # 👇 Convert bytes to string
            # str_data = bytes_data.decode('unicode_escape')

            fn = os.path.basename(file.filename)
            open(fn, 'wb').write(file.read())
            
            return FILE_PATH, 201
        except Exception as e:
            print(e)

        # data = request.get_json()
        # try: 
        #     file = request.files['file']
        #     password = ""
           
        #     if request.form.get('password') is not None:
        #         password = request.form.get('password')
           
        #     # strip the leading path from the file name
        #     fn = os.path.basename(file.filename)
        #     # open read and write the file into the server
        #     open(fn, 'wb').write(file.read())
        #     FILE_PATH = './' + file.filename

        #     text = ""
        #     with open(FILE_PATH, mode='rb') as f:
        #         reader = PdfReader(f)
        #         if reader.is_encrypted and not password:
        #             text = "only_encrypted_disallow"
        #         elif reader.is_encrypted and password: 
        #             reader.decrypt(password)
        #             for page in reader.pages:
        #                 text += page.extract_text()
        #         else:
        #             for page in reader.pages:
        #                 text += page.extract_text()
            
        #     return text, 201
        # except:
        #     print("somethign went wrong")
        #     return "error", 201
  
class Csv(Resource):

    def post(self):
            
        try:
            FILE_PATH = ""
            prompt = ""

            if request.form.get("prompt"):
                prompt = request.form.get("prompt")
            if request.form.get("FILE_PATH"):
                FILE_PATH = request.form.get("FILE_PATH")
            
            if FILE_PATH:
                agent = create_csv_agent(
                                OpenAI(openai_api_key=openai_api_key, temperature=0),
                                FILE_PATH,
                                verbose=True)
                res = agent.run(prompt)
                return res, 201

            # Upload CSV file with utf encoding
            file = request.files['file']
            FILE_PATH = './' + file.filename

            bytes_data = file.read()
            # 👇 Convert bytes to string
            str_data = bytes_data.decode('unicode_escape')

            fn = os.path.basename(file.filename)
            open(fn, 'w').write(str_data)
            
            return FILE_PATH, 201
        except Exception as e:
            print(e)


class Excel(Resource):
        def post(self):
            
            FILE_PATH = []
            FILE=""
            prompt = ""
            res = ""
            
            try:
                if request.form.get("prompt"):
                    prompt = request.form.get("prompt")
                if request.form.get("FILE_PATH"): 
                # and len(request.form.get("FILE_PATH")) > 0:
                    FILE = request.form.get("FILE_PATH")
                
                if FILE:
                    # for path in FILE_PATH.split(','):
                    agent = create_csv_agent(
                                    OpenAI(openai_api_key=openai_api_key, temperature=0),
                                    FILE,
                                    verbose=True)
                    res = agent.run(prompt)
                        
                    return res, 201

                # ---- excel to csv generation logic ----

                file = request.files['file']
                xls_file = file.filename
                fn = os.path.basename(xls_file)
                open(fn, 'wb').write(file.read())

                tabs = pd.ExcelFile(file).sheet_names 

                for tab in tabs:
                    output_csv = tab + '.csv'
                    FILE_PATH.append('./' +  output_csv)

                    # Read the XLS file using pandas and openpyxl as the engine
                    data = pd.read_excel(xls_file, tab, engine='openpyxl')
                    # Save the data as a CSV file
                    data.to_csv(output_csv, index=False)

                df = pd.concat(map(pd.read_csv, FILE_PATH), ignore_index=True)
                final_csv = './'+file.filename.split('.')[0]+'.csv'
                df.to_csv(final_csv, index=False)
                
                return final_csv, 201

            except Exception as e:
                print(e)

       
                
# adding the defined resources along with their corresponding urls
api.add_resource(Pdf, '/pdf')
api.add_resource(Csv, '/csv')
api.add_resource(Excel, '/excel')
  
# driver function
if __name__ == '__main__':
  
    app.run(debug = True, port=8000)